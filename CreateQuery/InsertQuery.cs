﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CreateQuery
{
    public class InsertQuery : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            try
            {
                //Se obtiene el Contexto de ejecución del Plugin
                IPluginExecutionContext context =
                    (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                IOrganizationServiceFactory serviceFactory =
                    (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

                //se obtiene el servicio de organización
                IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
                
                //se valida si el parametro de entrada es una Entidad
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    //se obtiene la entidad oportunidad del parametro de entrada
                    Entity goalRollupQuery = (Entity) context.InputParameters["Target"];
                    goalRollupQuery["queryentitytype"] = "ids_avancedecuota";
                    goalRollupQuery["fetchxml"] = @"<fetch mapping=""logical"" version=""1.0""><entity name=""ids_avancedecuota""><attribute name=""ids_name"" /><filter><condition attribute=""ids_name"" operator=""eq"" value=""AAS/EC/GALT/GALT160601/GALT6568/Inter-002/2016/Anexo A-OC 658/Vigente/2016-10-3"" /></filter></entity></fetch>";

                    service.Update(goalRollupQuery);
                }
            }
            catch (FaultException<OrganizationServiceFault> exc)
            {
                throw new InvalidPluginExecutionException("Error al crear query " + exc.Message, exc);
            }
            catch (InvalidPluginExecutionException exc)
            {
                throw new InvalidPluginExecutionException("Error al crear query " + exc.Message, exc);
            }
        }
    }
}
